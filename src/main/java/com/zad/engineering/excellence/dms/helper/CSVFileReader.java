package com.zad.engineering.excellence.dms.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Component;


@Component
public class CSVFileReader {
    
    BufferedReader fileReader = null;    
    
    public Collection<String> readCSVData(String fileToParse) {
        Collection<String> listOfLines = new ArrayList<String>(); 
        try
        {
            String line ="";
            int lineCount =1;
            fileReader = new BufferedReader(new FileReader(fileToParse));
            while ((line = fileReader.readLine()) != null) 
            {
                if(lineCount==1) {
                    lineCount++;
                    continue;                    
                }
                listOfLines.add(line);
            }
            
            return listOfLines;
        } 
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally
        {
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return listOfLines;
    }
    
}
