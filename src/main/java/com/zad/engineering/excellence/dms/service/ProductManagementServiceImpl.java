package com.zad.engineering.excellence.dms.service;

import static com.zad.engineering.excellence.dms.constants.ApplicationConstant.DELIMETER;
import static com.zad.engineering.excellence.dms.constants.ApplicationConstant.PRODUCT_PATH;
import static com.zad.engineering.excellence.dms.constants.ApplicationConstant.PRODUCT_HEADER;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zad.engineering.excellence.dms.model.Product;
import com.zad.engineering.excellence.dms.processor.DataProcessorImpl;

@Service
public class ProductManagementServiceImpl implements ProductManagementService {

    @Autowired
    private DataProcessorImpl dataProcessor;

    @Override
    public Product getProductById(String productId) {
        Collection<Product> productList = getAllProducts();
        Collection<Product> filterProductList = productList.stream()
                .filter(product -> product.getProductId().equalsIgnoreCase(productId)).collect(Collectors.toList());
        return filterProductList.stream().findFirst().orElse(new Product());

    }

    @Override
    public Collection<Product> getAllProducts() {
        Collection<String> listOfLines = dataProcessor.ReadData(PRODUCT_PATH);
        return listOfLines.stream().map(line -> extractProductData(line)).collect(Collectors.toList());

    }

    @Override
    public void addProduct(Product productData) {
        String lineToWrite  = buildProductDataAsString(productData);
        dataProcessor.WriteData(PRODUCT_PATH, lineToWrite);

    }

    @Override
    public void updateProduct(Product productData) {
        Collection<Product> productList = getAllProducts();
        if (isProductExist(productList, productData.getProductId())) {
            Product product = productList.stream()
                    .filter(prod -> prod.getProductId().equalsIgnoreCase(productData.getProductId())).findFirst().get();
            
            productList.remove(product);
            productList.add(productData);
            dataProcessor.WriteFirstLineData(PRODUCT_PATH, PRODUCT_HEADER);
            productList.stream().forEach(prod -> dataProcessor.WriteData(PRODUCT_PATH, buildProductDataAsString(prod)));
        }

    }

    @Override
    public void deleteProduct(String productId) {
        Collection<Product> productList = getAllProducts();
        if (isProductExist(productList, productId)) {
            Product product = productList.stream()
                    .filter(prod -> prod.getProductId().equalsIgnoreCase(productId)).findFirst().get();
            
            productList.remove(product);
            dataProcessor.WriteFirstLineData(PRODUCT_PATH, PRODUCT_HEADER);
            productList.stream().forEach(prod -> dataProcessor.WriteData(PRODUCT_PATH, buildProductDataAsString(prod)));
        }

    }
    
    private String buildProductDataAsString(Product productData) {
        return productData.getProductId() + "," + productData.getProductName() + "," + productData.getProductPrice() + ","
                + productData.getProductDesc() + "," + productData.getProductQuantity();
    }

    private Product extractProductData(String line) {
        Product product = new Product();
        String[] tokens = line.split(DELIMETER);
        product.setProductId(tokens[0]);
        product.setProductName(tokens[1]);
        product.setProductPrice(Integer.parseInt(tokens[2]));
        product.setProductDesc(tokens[3]);
        product.setProductQuantity(tokens[4]);
        return product;
    }
    
    private boolean isProductExist(Collection<Product> productList, String productId) {
        return productList.stream().filter(product -> product.getProductId().equalsIgnoreCase(productId)).findFirst().isPresent();
    }


}
