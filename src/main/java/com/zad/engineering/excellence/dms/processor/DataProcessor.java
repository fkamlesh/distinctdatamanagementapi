package com.zad.engineering.excellence.dms.processor;

import java.util.Collection;

public interface DataProcessor {
    
    public Collection<String> ReadData(String fileToParse);
    
    public String  WriteData(String fileToParse,String outputData);
    
    public String  WriteFirstLineData(String fileToParse,String outputData);
            
}
