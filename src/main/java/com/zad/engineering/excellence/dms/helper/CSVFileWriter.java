package com.zad.engineering.excellence.dms.helper;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.stereotype.Component;

@Component
public class CSVFileWriter {

    BufferedWriter fileWriter = null;

    public String writeToCSVFile(String fileToWrite, String lineToWrite) {
        try {
            fileWriter = new BufferedWriter(new FileWriter(fileToWrite,true));
            fileWriter.newLine();
            fileWriter.append(lineToWrite);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "failed";
    }
    
    public String writeFirstLineToCSVFile(String fileToWrite, String lineToWrite) {
        try {
            fileWriter = new BufferedWriter(new FileWriter(fileToWrite));
            fileWriter.append(lineToWrite);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "failed";
    }

}
