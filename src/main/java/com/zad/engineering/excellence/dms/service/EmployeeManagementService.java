package com.zad.engineering.excellence.dms.service;

import java.util.Collection;

import com.zad.engineering.excellence.dms.model.Employee;

public interface EmployeeManagementService {
    
    public Employee getAllEmployeeById(String employeeId);
    
    public Collection<Employee> getAllEmployees();
    
    public void addEmployee(Employee employeeData);
    
    public void updateEmployee(Employee employeeData);
        
    public void deleteEmployee(String employeeData);

}
