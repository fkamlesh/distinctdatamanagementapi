package com.zad.engineering.excellence.dms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistinctDataManagementAPIApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistinctDataManagementAPIApplication.class, args);
	}

}
