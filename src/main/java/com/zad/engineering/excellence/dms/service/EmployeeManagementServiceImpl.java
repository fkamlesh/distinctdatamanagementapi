package com.zad.engineering.excellence.dms.service;

import static com.zad.engineering.excellence.dms.constants.ApplicationConstant.DELIMETER;
import static com.zad.engineering.excellence.dms.constants.ApplicationConstant.EMPLOYEE_PATH;
import static com.zad.engineering.excellence.dms.constants.ApplicationConstant.EMPLOYEE_HEADER;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zad.engineering.excellence.dms.model.Employee;
import com.zad.engineering.excellence.dms.processor.DataProcessorImpl;

@Service
public class EmployeeManagementServiceImpl implements EmployeeManagementService {

    @Autowired
    private DataProcessorImpl dataProcessor;

    @Override
    public Employee getAllEmployeeById(String employeeId) {

        Collection<Employee> employeeList = getAllEmployees();
        int empId = Integer.parseInt(employeeId);
        Collection<Employee> filterEmpList = employeeList.stream().filter(emp -> emp.getEmployeeId() == empId)
                .collect(Collectors.toList());
        return filterEmpList.stream().findFirst().orElse(new Employee());
    }

    @Override
    public Collection<Employee> getAllEmployees() {
        Collection<String> listOfLines = dataProcessor.ReadData(EMPLOYEE_PATH);
        return listOfLines.stream().map(line -> extractEmployeeData(line)).collect(Collectors.toList());
    }

    private Employee extractEmployeeData(String line) {
        Employee employee = new Employee();
        String[] tokens = line.split(DELIMETER);
        employee.setEmployeeId(Integer.parseInt(tokens[0]));
        employee.setFirstName(tokens[1]);
        employee.setLastName(tokens[2]);
        employee.setEmail(tokens[3]);
        employee.setPhoneNumber(tokens[4]);
        employee.setHireDate(tokens[5]);
        employee.setSalary(Integer.parseInt(tokens[6]));
        employee.setManagerId(Integer.parseInt(tokens[7]));
        employee.setDepartmentId(Integer.parseInt(tokens[8]));
        return employee;

    }

    @Override
    public void addEmployee(Employee employeeData) {
        String lineToWrite = buildEmployeeDataAsString(employeeData);
        dataProcessor.WriteData(EMPLOYEE_PATH, lineToWrite);

    }

    private String buildEmployeeDataAsString(Employee employeeData) {
        return employeeData.getEmployeeId() + "," + employeeData.getFirstName() + "," + employeeData.getLastName() + ","
                + employeeData.getEmail() + "," + employeeData.getPhoneNumber() + "," + employeeData.getHireDate() + ","
                + employeeData.getSalary() + "," + employeeData.getManagerId() + "," + employeeData.getDepartmentId();

    }

    @Override
    public void updateEmployee(Employee employeeData) {
        Collection<Employee> employeeList = getAllEmployees();
        if (isEmployeeExist(employeeList, employeeData.getEmployeeId())) {
            Employee employee = employeeList.stream().filter(emp -> emp.getEmployeeId() == employeeData.getEmployeeId())
                    .findFirst().get();
            employeeList.remove(employee);
            employeeList.add(employeeData);
            dataProcessor.WriteFirstLineData(EMPLOYEE_PATH, EMPLOYEE_HEADER);
            employeeList.stream().forEach(emp -> dataProcessor.WriteData(EMPLOYEE_PATH, buildEmployeeDataAsString(emp)));
        }
    }

    private boolean isEmployeeExist(Collection<Employee> employeeList, int employeeId) {
        return employeeList.stream().filter(emp -> emp.getEmployeeId() == employeeId).findFirst().isPresent();
    }

    @Override
    public void deleteEmployee(String employeeId) {
        Collection<Employee> employeeList = getAllEmployees();
        int empId = Integer.parseInt(employeeId);
        if (isEmployeeExist(employeeList, empId)) {
            Employee employee = employeeList.stream().filter(emp -> emp.getEmployeeId() == empId).findFirst().get();
            employeeList.remove(employee);
            dataProcessor.WriteFirstLineData(EMPLOYEE_PATH, EMPLOYEE_HEADER);
            employeeList.stream().forEach(emp -> dataProcessor.WriteData(EMPLOYEE_PATH, buildEmployeeDataAsString(emp)));
        }
    }

}
