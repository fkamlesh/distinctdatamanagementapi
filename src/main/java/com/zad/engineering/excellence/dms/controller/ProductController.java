package com.zad.engineering.excellence.dms.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.zad.engineering.excellence.dms.model.Product;
import com.zad.engineering.excellence.dms.service.ProductManagementServiceImpl;

@RestController
public class ProductController {
    
    @Autowired
    private ProductManagementServiceImpl productManagementService;
    
    @GetMapping("/products/{productId}")
    public ResponseEntity<Product> getProductById(@PathVariable String productId) {
        Product product = productManagementService.getProductById(productId);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @GetMapping("/products")
    public ResponseEntity<Collection<Product>> getAllProducts() {
        Collection<Product> productList = productManagementService.getAllProducts();
        return new ResponseEntity<>(productList, HttpStatus.OK);
    }
    
    
    @PostMapping("/products")
    public ResponseEntity<String> addNewProduct(@RequestBody Product productData){
        productManagementService.addProduct(productData);
        return new ResponseEntity<>("Successfully Added", HttpStatus.CREATED);
    }
    
    @PutMapping("/products")
    public ResponseEntity<String> updateProduct(@RequestBody Product productData){
        productManagementService.updateProduct(productData);
        return new ResponseEntity<>("Successfully updated", HttpStatus.OK);
    }
    
    @DeleteMapping("/products/{productId}")
    public ResponseEntity<String> deleteProduct(@PathVariable String productId){
        productManagementService.deleteProduct(productId);
        return new ResponseEntity<>("Successfully Deleted", HttpStatus.OK);
    }

}
