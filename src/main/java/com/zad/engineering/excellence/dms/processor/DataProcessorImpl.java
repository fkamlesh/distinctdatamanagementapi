package com.zad.engineering.excellence.dms.processor;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zad.engineering.excellence.dms.helper.CSVFileReader;
import com.zad.engineering.excellence.dms.helper.CSVFileWriter;

@Component
public class DataProcessorImpl implements DataProcessor {

    @Autowired
    private CSVFileReader csvFileReader;

    @Autowired
    private CSVFileWriter csvFileWriter;

    @Override
    public Collection<String> ReadData(String fileToParse) {
        return csvFileReader.readCSVData(fileToParse);
    }

    @Override
    public String WriteData(String fileToParse, String lineToWrite) {
        return csvFileWriter.writeToCSVFile(fileToParse, lineToWrite);
    }

    @Override
    public String WriteFirstLineData(String fileToParse, String lineToWrite) {        
        return csvFileWriter.writeFirstLineToCSVFile(fileToParse, lineToWrite);
    }

}
