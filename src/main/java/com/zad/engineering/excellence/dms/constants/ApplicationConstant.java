package com.zad.engineering.excellence.dms.constants;

public class ApplicationConstant {

    public static final String EMPLOYEE_PATH = "C:/ZahidWork/Personal/Codebase/distinctdatamanagementapi/datafiles/employee.csv";
    public static final String PRODUCT_PATH = "C:/ZahidWork/Personal/Codebase/distinctdatamanagementapi/datafiles/product.csv";
    public static final String DELIMETER = ",";
    public static final String EMPLOYEE_HEADER = "EMPLOYEE_ID,FIRST_NAME,LAST_NAME,EMAIL,PHONE_NUMBER,HIRE_DATE,SALARY,MANAGER_ID,DEPARTMENT_ID";
    public static final String PRODUCT_HEADER = "PRODUCT_ID,PRODUCT_NAME,PRODUCT_PRICE,PRODUCT_DESC,PRODUCT_QUANTITY";

}
