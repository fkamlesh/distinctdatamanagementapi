package com.zad.engineering.excellence.dms.service;

import java.util.Collection;
import com.zad.engineering.excellence.dms.model.Product;

public interface ProductManagementService {
    
 public Product getProductById(String productId);
    
    public Collection<Product> getAllProducts();
    
    public void addProduct(Product productData);
    
    public void updateProduct(Product productData);
        
    public void deleteProduct(String productId);

}
