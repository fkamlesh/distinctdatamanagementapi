package com.zad.engineering.excellence.dms.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.zad.engineering.excellence.dms.model.Employee;
import com.zad.engineering.excellence.dms.service.EmployeeManagementServiceImpl;


@RestController
public class EmployeeController {

    @Autowired
    private EmployeeManagementServiceImpl employeeManagementService;
    
    @GetMapping("/employees/{employeeId}")
    public ResponseEntity<Employee> getAllEmployees(@PathVariable String employeeId) {
        Employee employee = employeeManagementService.getAllEmployeeById(employeeId);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @GetMapping("/employees")
    public ResponseEntity<Collection<Employee>> getAllEmployees() {
        Collection<Employee> employeeList = employeeManagementService.getAllEmployees();
        return new ResponseEntity<>(employeeList, HttpStatus.OK);
    }
    
    
    @PostMapping("/employees")
    public ResponseEntity<String> addNewEmployee(@RequestBody Employee employee){
        employeeManagementService.addEmployee(employee);
        return new ResponseEntity<>("Successfully Added", HttpStatus.OK);
    }
    
    @PutMapping("/employees")
    public ResponseEntity<String> updateEmployee(@RequestBody Employee employee){
        employeeManagementService.updateEmployee(employee);
        return new ResponseEntity<>("Successfully updated", HttpStatus.OK);
    }
    
    @DeleteMapping("/employees/{employeeId}")
    public ResponseEntity<String> deleteEmployee(@PathVariable String employeeId){
        employeeManagementService.deleteEmployee(employeeId);
        return new ResponseEntity<>("Successfully Deleted", HttpStatus.OK);
    }

}
